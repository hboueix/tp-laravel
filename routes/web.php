<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/presentation', function () {
    return view('presentation');
});

Route::get('/contact', function () {
    return view('contact');
})-> name('contact');

Route::get('/autres', function () {
    return view('autres');
})-> name('autres');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => ['role:admin']], function () {
    Route::get('/admin', 'adminController@index')->name('admin');
});

Route::group(['middleware' => ['role:dev']], function () {
    Route::get('/dev', 'devController@index')->name('dev');
});

Route::group(['middleware' => ['role:writer']], function () {
    Route::get('/writer', 'writerController@index')->name('writer');
});

