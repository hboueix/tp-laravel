- ## TP Laravel
    - [Installation de Laravel](/{{route}}/{{version}}/overview)
    - [Premier projet](/{{route}}/{{version}}/creation)
    - [Gestion des rôles](/{{route}}/{{version}}/roles)
    - [Middlewares](/{{route}}/{{version}}/middleware)