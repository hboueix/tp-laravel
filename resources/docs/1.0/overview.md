# Installation de Laravel

---

- 

Pré-requis :
- Installer ou avoir un IDE (PhpStorm, VSCode, Sublime Text...)

- Installer ou avoir PHP 7

- Installer ou avoir composer

- Installer ou avoir un serveur web

- Installer ou avoir une base de données  
  
On va utiliser XAMPP. Dans le dossier `xampp`, lancez un terminal puis créez un nouveau projet avec la commande :  
```php  
composer create-project --prefer-dist laravel/laravel tp-laravel
```  
  
On peut désormais démarrer notre serveur de développement PHP via la commande :  
```php  
php artisan serve
```  
  
On peut également installer Larecipe pour de la documentation :  
```php  
composer require binarytorch/larecipe  
    
php artisan larecipe:install
```  

Et accèder à notre projet à l'adresse `localhost:8000`.