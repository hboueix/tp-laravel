# Utilisation des middlewares

--- 

- 

## Créer des controlleurs

---

On va commencer par créer 3 controlleurs, un pour chaque rôle, à l'aide de la commande `make:controller` :
```php
php artisan make:controller AdminController
php artisan make:controller DevController
php artisan make:controller WriterController
```
Dans chacun des fichiers que l'on vient de créer, on va ajouter une fonction index() qui va renvoyer la view que l'on veut. On crée donc 3 nouvelles views ; une pour chaque rôle. Pour le rôle admin par exemple, on va écrire dans `AdminController.php` :
```php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function index() 
    {
        return view('admin');
    }
}
```

## Routes vers controlleurs

--- 

Pour que l'utilisateur soit rediriger en fonction de son rôle vers le bon controlleur, on va utiliser des routes :
```php
Route::group(['middleware' => ['role:admin']], function () {
    Route::get('/admin', 'adminController@index')->name('admin');
});

Route::group(['middleware' => ['role:dev']], function () {
    Route::get('/dev', 'devController@index')->name('dev');
});

Route::group(['middleware' => ['role:writer']], function () {
    Route::get('/writer', 'writerController@index')->name('writer');
```