# Authentification avec gestion des rôles

--- 

- 


## Installation de spatie

--- 

Pour gèrer des rôles, on va utiliser le package `spatie/laravel-permissions` :
```php
composer require spatie/laravel-permission
```
Il faut ensuite publier la migration : 
```php
php artisan vendor:publish --provider="Spatie\Permission\PermissionServiceProvider" --tag="migrations"
```
Une fois la migration publiée, on peut créer les tables rôles et permissions avec la commande `migrate`.  
On peut maintenant publier le fichier de config :
```php
php artisan vendor:publish --provider="Spatie\Permission\PermissionServiceProvider" --tag="config"
```

## Basic usage

--- 

Tout d'abord, il faut ajouter le trait `Spatie\Permission\Traits\HasRoles` à notre modèle d'User dans `app/User.php` :
```php
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasRoles;

    // ...
}
```
On va ensuite créer un seeder avec la commande que l'on a vu précedemment :
```php
php artisan make:seeder RolesTableSeeder
```
Et on oublie pas de rajouter dans notre fichier `DatabaseSeeder.php` l'appel du seeder que l'on vient de créer, **AVANT** l'appel du seeder des utilisateurs :
```php
$this->call(RolesTableSeeder::class);
$this->call(UsersTableSeeder::class);
// L'ordre est IMPORTANT !
```
On a maintenant un nouveau fichier : `RolesTableSeeder.php`, dans lequel il faut d'abord rajouter les lignes :
```php
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
```
Puis dans la fonction run() :
```php
Role::create(['name' => 'admin']);
Role::create(['name' => 'dev']);
Role::create(['name' => 'writer']);
```
On peut désormais lancer nos seeders avec la commande `db:seed`.


## Donner des rôles aux utilisateurs

---

On va maintenant modifier notre `UsersTableSeeder`, pour qu'il crée :
- 10 users avec le rôle admin
- 40 users avec le rôle dev
- 50 users avec le rôle writer  
  
On va donc modifier la fonction run() notre fichier pour qu'elle contienne :
```php
factory(App\User::class, 10)->create()->each( function($user) {
    $user->assignRole('admin');
});
factory(App\User::class, 40)->create()->each( function($user) {
    $user->assignRole('dev');
});
factory(App\User::class, 50)->create()->each( function($user) {
    $user->assignRole('writer');
});
```
On recontruit notre database avec des rôles pour chaque user avec la commande :
```php
php artisan migrate:refresh --seed
```
On peut vérifier les changements dans notre database avec la table `model_has_roles`.  

