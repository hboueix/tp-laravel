# Création d'un premier projet

--- 

- 

## Créer des views et des routes

---

Les views sont les différentes pages de notre site. On les trouve dans le dossier `ressources/views`, il faut les nommer en `.blade.php` systèmatiquement.  
Pour faire des liens entre elles, il faut créer des routes dans le fichier `routes/web.php` de cette façon :  
```php  
Route::get('/presentation', function () {
    return view('presentation');
});
```
Et les fichiers de views en ajoutant des liens :  
{{ url('/') }}
ou  
`{{ route('contact') }}



## Créer des seeders

--- 

Pour générer un seeder, il faut utiliser la commande `make:seeder` :  
```php
php artisan make:seeder UsersTableSeeder
```  
La table `UsersTableSeeder` sera placée dans le dossier `database/seeds`.
Dans ce même dossier il faut tout d'abord modifier le fichier `DatabaseSeeder.php` et rajouter dans la fonction run() :  
```php
$this->call(UsersTableSeeder::class);
```  
On va maintenant utiliser le model factory `UserFactory.php` que l'on va appeler dans le fichier `UsersTableSeeder.php` en rajoutant dans la fonction run() :  
```php 
factory(App\User::class, 100)->create();
```  
Après avoir écrit nos seeders, il est utile de regénérer l'autoloader de Composer avec la commande :  
```php
composer dump-autoload
```
Ensuite on peut utiliser la `db:seed` artisan command pour remplir notre database. Par défaut la commande appelle le fichier `DatabaseSeeder.php` qui dans notre cas, appelle le fichier `UsersTableSeeder.php`, sinon on peut utiliser l'option `--class` :
```php 
php artisan db:seed

php artisan db:seed --class=UsersTableSeeder
```
On peut également la remplir avec la commande `migrate:refresh` qui va drop toutes nos tables et relancer nos migrations. Cette commande va totalement reconstruire notre database :  
```php
php artisan migrate:fresh --seed
```

## Authentification

--- 

Pour obtenir une interface d'authentification, on peut utiliser le package `laravel/ui` :
```php
composer require laravel/ui --dev

php artisan ui vue --auth
```
Un fichier `HomeController.php` est généré dans le dossier `app/Http/Controllers`, il gérera les requêtes post-login.