# TP-laravel

- [Installation de Laravel](/{{route}}/{{version}}/overview)
- [Premier projet](/{{route}}/{{version}}/creation)
- [Gestion des rôles](/{{route}}/{{version}}/roles)
- [Middlewares](/{{route}}/{{version}}/middleware)

--- 

## Installation de Laravel

Pré-requis :
- Installer ou avoir un IDE (PhpStorm, VSCode, Sublime Text...)

- Installer ou avoir PHP 7

- Installer ou avoir composer

- Installer ou avoir un serveur web

- Installer ou avoir une base de données  
  
On va utiliser XAMPP. Dans le dossier `xampp`, lancez un terminal puis créez un nouveau projet avec la commande :  
```php  
composer create-project --prefer-dist laravel/laravel tp-laravel
```  
  
On peut désormais démarrer notre serveur de développement PHP via la commande :  
```php  
php artisan serve
```  
  
On peut également installer Larecipe pour de la documentation :  
```php  
composer require binarytorch/larecipe  
    
php artisan larecipe:install
```  

Et accèder à notre projet à l'adresse `localhost:8000`.

---

## Création d'un premier projet

 

### Créer des views et des routes

Les views sont les différentes pages de notre site. On les trouve dans le dossier `ressources/views`, il faut les nommer en `.blade.php` systèmatiquement.  
Pour faire des liens entre elles, il faut créer des routes dans le fichier `routes/web.php` de cette façon :  
```php  
Route::get('/presentation', function () {
    return view('presentation');
});
```
Et les fichiers de views en ajoutant des liens :  
{{ url('/') }}
ou  
`{{ route('contact') }}



### Créer des seeders

Pour générer un seeder, il faut utiliser la commande `make:seeder` :  
```php
php artisan make:seeder UsersTableSeeder
```  
La table `UsersTableSeeder` sera placée dans le dossier `database/seeds`.
Dans ce même dossier il faut tout d'abord modifier le fichier `DatabaseSeeder.php` et rajouter dans la fonction run() :  
```php
$this->call(UsersTableSeeder::class);
```  
On va maintenant utiliser le model factory `UserFactory.php` que l'on va appeler dans le fichier `UsersTableSeeder.php` en rajoutant dans la fonction run() :  
```php 
factory(App\User::class, 100)->create();
```  
Après avoir écrit nos seeders, il est utile de regénérer l'autoloader de Composer avec la commande :  
```php
composer dump-autoload
```
Ensuite on peut utiliser la `db:seed` artisan command pour remplir notre database. Par défaut la commande appelle le fichier `DatabaseSeeder.php` qui dans notre cas, appelle le fichier `UsersTableSeeder.php`, sinon on peut utiliser l'option `--class` :
```php 
php artisan db:seed

php artisan db:seed --class=UsersTableSeeder
```
On peut également la remplir avec la commande `migrate:refresh` qui va drop toutes nos tables et relancer nos migrations. Cette commande va totalement reconstruire notre database :  
```php
php artisan migrate:fresh --seed
```

### Authentification 

Pour obtenir une interface d'authentification, on peut utiliser le package `laravel/ui` :
```php
composer require laravel/ui --dev

php artisan ui vue --auth
```
Un fichier `HomeController.php` est généré dans le dossier `app/Http/Controllers`, il gérera les requêtes post-login.

----


## Authentification avec gestion des rôles


### Installation de spatie

Pour gèrer des rôles, on va utiliser le package `spatie/laravel-permissions` :
```php
composer require spatie/laravel-permission
```
Il faut ensuite publier la migration : 
```php
php artisan vendor:publish --provider="Spatie\Permission\PermissionServiceProvider" --tag="migrations"
```
Une fois la migration publiée, on peut créer les tables rôles et permissions avec la commande `migrate`.  
On peut maintenant publier le fichier de config :
```php
php artisan vendor:publish --provider="Spatie\Permission\PermissionServiceProvider" --tag="config"
```

### Basic usage

Tout d'abord, il faut ajouter le trait `Spatie\Permission\Traits\HasRoles` à notre modèle d'User dans `app/User.php` :
```php
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasRoles;

    // ...
}
```
On va ensuite créer un seeder avec la commande que l'on a vu précedemment :
```php
php artisan make:seeder RolesTableSeeder
```
Et on oublie pas de rajouter dans notre fichier `DatabaseSeeder.php` l'appel du seeder que l'on vient de créer, **AVANT** l'appel du seeder des utilisateurs :
```php
$this->call(RolesTableSeeder::class);
$this->call(UsersTableSeeder::class);
// L'ordre est IMPORTANT !
```
On a maintenant un nouveau fichier : `RolesTableSeeder.php`, dans lequel il faut d'abord rajouter les lignes :
```php
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
```
Puis dans la fonction run() :
```php
Role::create(['name' => 'admin']);
Role::create(['name' => 'dev']);
Role::create(['name' => 'writer']);
```
On peut désormais lancer nos seeders avec la commande `db:seed`.


### Donner des rôles aux utilisateurs

On va maintenant modifier notre `UsersTableSeeder`, pour qu'il crée :
- 10 users avec le rôle admin
- 40 users avec le rôle dev
- 50 users avec le rôle writer  
  
On va donc modifier la fonction run() notre fichier pour qu'elle contienne :
```php
factory(App\User::class, 10)->create()->each( function($user) {
    $user->assignRole('admin');
});
factory(App\User::class, 40)->create()->each( function($user) {
    $user->assignRole('dev');
});
factory(App\User::class, 50)->create()->each( function($user) {
    $user->assignRole('writer');
});
```
On recontruit notre database avec des rôles pour chaque user avec la commande :
```php
php artisan migrate:refresh --seed
```
On peut vérifier les changements dans notre database avec la table `model_has_roles`.  

---

## Utilisation des middlewares

### Créer des controlleurs

On va commencer par créer 3 controlleurs, un pour chaque rôle, à l'aide de la commande `make:controller` :
```php
php artisan make:controller AdminController
php artisan make:controller DevController
php artisan make:controller WriterController
```
Dans chacun des fichiers que l'on vient de créer, on va ajouter une fonction index() qui va renvoyer la view que l'on veut. On crée donc 3 nouvelles views ; une pour chaque rôle. Pour le rôle admin par exemple, on va écrire dans `AdminController.php` :
```php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function index() 
    {
        return view('admin');
    }
}
```

### Routes vers controlleurs

Pour que l'utilisateur soit rediriger en fonction de son rôle vers le bon controlleur, on va utiliser des routes :
```php
Route::group(['middleware' => ['role:admin']], function () {
    Route::get('/admin', 'adminController@index')->name('admin');
});

Route::group(['middleware' => ['role:dev']], function () {
    Route::get('/dev', 'devController@index')->name('dev');
});

Route::group(['middleware' => ['role:writer']], function () {
    Route::get('/writer', 'writerController@index')->name('writer');
```

--- 

## Créer un profile

On va créer un nouveau model `Profile` et lui associer un controller (qui intègrera les focntions d'un CRUD), une factory et une migration :
```php
php artisan make:model Profile -a
// L'option -a créé controller, factory et migration
```

On va maintenant créer une fonction `hasProfile()`et `hasUser()` respectivement dans les fichiers `app/User.php` et `app/Profile.php` :
```php
// dans app/User.php
public function hasProfile() {
    return $this->hasOne('App\Profile');
}

// dans app/Profile.php
public function hasUser() {
    return $this->belongsTo('App\User');
}
```

Ensuite dans le fichier de factory `ProfileFactory.php`, on modifie la fonction `define()` pour definir notre profile :
```php
$factory->define(Profile::class, function (Faker $faker) {
    return [
        'last_name' => $faker->lastName,
        'first_name' => $faker->firstName,
        'age' => $faker->dateTime,
        'phone_number' => $faker->phoneNumber,
        'address' => $faker->address,
    ];
});
```

Une fois notre factory crée, on va pouvoir modifier la fonction `up()` de notre fichier de migration `*_create_profiles_table.php` :
```php
public function up()
{
    Schema::create('profiles', function (Blueprint $table) {
        $table->bigIncrements('id');
        $table->string('last_name');
        $table->string('first_name');
        $table->dateTime('age');
        $table->string('phone_number');
        $table->string('address');
        $table->timestamps();
    });
}
```

On peut désormais créé un seeder :
```php 
php artisan make:seeder ProfilesTableSeeder
```
Et dans sa fonction `run()` on va créer des profiles à partir de la factory :
```php
factory(App\Profile::class, 100)->create()->each(function ($user) {
    $user->hasProfile;
});
```
Ne pas oublier la ligne qui appelle notre seeder dans `DatabaseTableSeeder.php` :
```php
public function run()
{
    $this->call(RolesTableSeeder::class);
    $this->call(UsersTableSeeder::class);
    $this->call(ProfilesTableSeeder::class);
}
```
Enfin on peut mettre à jour toute notre base de données :
```php
php artisan migrate:refresh --seed
```